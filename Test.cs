using Godot;
using System;

public class Test : Button
{
	public class Model : Resource
	{
		[Export]
		public string Value {get; set;}
	}

	public void Run()
	{
		ResourceSaver.Save("res://cs_output.tres", new Model() { Value = "test"});
		
		var gd_resource = (Resource)GD.Load("res://model.gd").Call("new");
		gd_resource.Set("Value", "test");
		ResourceSaver.Save("res://gd_output.tres", gd_resource);
	}
}
